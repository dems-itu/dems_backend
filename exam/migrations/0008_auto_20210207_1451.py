# Generated by Django 3.1.6 on 2021-02-07 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("exam", "0007_examquestion_in_exam_results"),
    ]

    operations = [
        migrations.AddConstraint(
            model_name="response",
            constraint=models.UniqueConstraint(fields=("student", "exam_question"), name="unique_student_response"),
        ),
    ]

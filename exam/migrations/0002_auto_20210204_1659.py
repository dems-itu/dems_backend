# Generated by Django 3.1.6 on 2021-02-04 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("exam", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="question",
            name="choices",
            field=models.JSONField(default=dict, verbose_name="Choices"),
        ),
        migrations.AlterField(
            model_name="response",
            name="score_details",
            field=models.JSONField(default=dict, verbose_name="Score details"),
        ),
    ]

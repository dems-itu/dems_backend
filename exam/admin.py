from django.contrib import admin
from django.db.models import JSONField
from django.utils.translation import ugettext_lazy as _
from django_ace import AceWidget
from django_json_widget.widgets import JSONEditorWidget
from reversion.admin import VersionAdmin

from exam.models import CodeEvaluationTracker, Exam, ExamQuestion, Question, QuestionArtifact, Response


@admin.register(QuestionArtifact)
class QuestionArtifactAdmin(VersionAdmin):
    list_display = ("question", "updated_at")
    autocomplete_fields = ("question",)
    search_fields = ("id",)


class QuestionArtifactInlineAdmin(admin.TabularInline):
    model = QuestionArtifact


@admin.register(Question)
class QuestionAdmin(VersionAdmin):
    list_display = ("internal_title", "title", "question_type", "author", "updated_at", "created_at")
    list_filter = ("updated_at", "created_at")
    autocomplete_fields = ("author",)
    search_fields = (
        "internal_title",
        "title",
    )
    formfield_overrides = {JSONField: {"widget": JSONEditorWidget}}
    fieldsets = (
        (
            _("Question details"),
            {
                "fields": (
                    "internal_title",
                    ("title", "question_type"),
                    "description",
                    "allow_notes",
                )
            },
        ),
        (
            _("Choice and multi-choice details"),
            {
                "fields": (
                    "choices",
                    "correct_choice",
                )
            },
        ),
        (
            _("Text question details"),
            {"fields": ("max_text_length",)},
        ),
        (
            _("Coding question details"),
            {"fields": (("example_solution", "test_cases"), "test_template")},
        ),
        (
            _("Stamps"),
            {"fields": (("author", "updated_at", "created_at"),)},
        ),
    )
    readonly_fields = ("author", "updated_at", "created_at")
    inlines = [
        QuestionArtifactInlineAdmin,
    ]

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.save()

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ["example_solution", "test_cases", "test_template"]:
            kwargs["widget"] = AceWidget(theme="dracula", mode="python")
        return super().formfield_for_dbfield(db_field, **kwargs)


@admin.register(ExamQuestion)
class ExamQuestionAdmin(VersionAdmin):
    list_display = ("exam", "question", "rank", "points", "in_exam_results", "created_at")
    list_filter = ("created_at",)
    autocomplete_fields = ("exam", "question")
    search_fields = ("exam__title", "question__title")
    readonly_fields = ("created_at",)
    formfield_overrides = {JSONField: {"widget": JSONEditorWidget}}


class ExamQuestionInline(admin.TabularInline):
    model = ExamQuestion
    autocomplete_fields = ("question",)
    formfield_overrides = {JSONField: {"widget": JSONEditorWidget(height="200px", width="200px")}}


@admin.register(Exam)
class ExamAdmin(VersionAdmin):
    list_display = ("title", "classroom", "start_time", "end_time", "updated_at", "created_at")
    list_filter = ("updated_at", "created_at")
    autocomplete_fields = ("classroom",)
    search_fields = ("title",)
    inlines = [
        ExamQuestionInline,
    ]


@admin.register(Response)
class ResponseAdmin(VersionAdmin):
    list_display = ("student", "exam_question", "total_score", "updated_at", "created_at")
    list_filter = ("updated_at", "created_at")
    autocomplete_fields = ("student", "exam_question")
    search_fields = ("student__username",)
    formfield_overrides = {JSONField: {"widget": JSONEditorWidget}}


@admin.register(CodeEvaluationTracker)
class CodeEvaluationTrackerAdmin(VersionAdmin):
    list_display = ("id", "response", "started_at", "ended_at", "created_at")
    autocomplete_fields = ("response",)
    search_fields = ("id", "response__id")
    readonly_fields = ("started_at", "ended_at", "created_at")
    formfield_overrides = {JSONField: {"widget": JSONEditorWidget}}

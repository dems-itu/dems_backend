from django.test import TestCase

from classroom.models import Classroom, DegreeTypes
from exam.apps import ExamConfig
from user.models import User

# from exam.models import Exam, ExamQuestion, Question, QuestionTypes, Response


class ExamTestCase(TestCase):
    CRN = "1000101"
    CODE = "BLG101"

    def test_apps(self):
        self.assertEqual(ExamConfig.name, "exam")

    def test_create_exam(self):
        classroom = Classroom.objects.create(name=self.CRN, course_code=self.CODE, degree=DegreeTypes.BACHELOR)
        teacher = User.objects.create_user(username="0123456", is_teacher=True)
        assistant = User.objects.create_user(username="0123457", is_assistant=True)
        student = User.objects.create_user(username="123457")
        classroom.teachers.add(teacher, assistant)
        classroom.students.add(student)
        self.assertEqual(classroom.teacher_count, 2)
        self.assertEqual(classroom.students_count, 1)
        self.assertEqual(classroom.exams.count(), 0)
        # @TODO

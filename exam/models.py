import os
from typing import List
from uuid import uuid4

from django.db import models
from django.db.models import UniqueConstraint
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from markdownx.models import MarkdownxField

from classroom.models import Classroom
from user.models import User


class QuestionTypes(models.IntegerChoices):
    CHOICE = 1, _("Checkbox")
    MULTI_CHOICE = 2, _("Multiple choice")
    TEXT = 3, _("Text answer")
    CODE = 4, _("Coding")


def question_artifact_path(instance, filename):
    return f"questions/{instance.question.id}/{filename}"


class Question(models.Model):
    """Question details."""

    internal_title = models.CharField(
        _("Internal title"), max_length=200, help_text=_("Internal title is only shown to teachers and assistants.")
    )
    title = models.CharField(_("Title"), max_length=200, help_text=_("Headline of the question."))
    description = MarkdownxField(_("Description"), help_text=_("Description for the question. Use markdown syntax."))
    question_type = models.IntegerField(
        _("Question type"), choices=QuestionTypes.choices, default=QuestionTypes.CHOICE
    )
    # checkbox and multi-choice question properties
    correct_choice = models.CharField(
        _("Correct choice indexes"),
        help_text=_("Correct choice indexes separated by comma"),
        max_length=50,
        blank=True,
        null=True,
    )
    choices = models.JSONField(
        _("Choices"), default=dict, blank=True, help_text=_("JSON with key, value for checkbox options")
    )
    # text question properties
    max_text_length = models.IntegerField(
        _("Max response length"), default=255, help_text=_("Maximum length of the text question response.")
    )
    # coding question properties
    example_solution = models.TextField(
        _("Example solution"), blank=True, null=True, help_text=_("Example solution to verify test cases.")
    )
    test_cases = models.TextField(
        _("Test cases"),
        blank=True,
        null=True,
        help_text=_("Pytest style testing cases. Import user functions from the user_code module."),
    )
    test_template = models.TextField(
        _("Test template"), blank=True, null=True, help_text=_("Template that will be provided to students.")
    )
    # control notes
    allow_notes = models.BooleanField(
        _("Allow notes"), default=False, help_text=_("Allow students to add custom notes.")
    )
    # question author
    author = models.ForeignKey(
        User, related_name="questions", on_delete=models.SET_NULL, null=True, verbose_name=_("Author")
    )
    # timestamps
    updated_at = models.DateTimeField(_("Updated"), auto_now=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    def __str__(self) -> str:
        return self.title

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")
        ordering = ("-updated_at",)


class QuestionArtifact(models.Model):
    """
    Additional files required by the question.
    This is used for the coding questions where extra files (like txt) are required for test cases.
    """

    id = models.UUIDField(_("UUID"), primary_key=True, default=uuid4, editable=False)
    question = models.ForeignKey(
        Question, related_name="artifacts", on_delete=models.CASCADE, verbose_name=_("Question")
    )
    file = models.FileField(
        _("File"),
        upload_to=question_artifact_path,
        help_text=_("Files will be mounted to the test environment with the user and test codes."),
    )
    # timestamps
    updated_at = models.DateTimeField(_("Updated"), auto_now=True)

    def __str__(self) -> str:
        return str(self.id)

    @property
    def filename(self) -> str:
        return os.path.basename(self.file.name)

    class Meta:
        verbose_name = _("Artifact")
        verbose_name_plural = _("Artifacts")
        ordering = ("updated_at",)


class Exam(models.Model):
    """Exam details."""

    title = models.CharField(_("Title"), max_length=200, help_text=_("Headline of the exam."))
    classroom = models.ForeignKey(
        Classroom,
        related_name="exams",
        on_delete=models.CASCADE,
        verbose_name=_("Classroom"),
        help_text=_("Exams can only be attended by students in this classroom."),
    )
    start_time = models.DateTimeField(_("Starts at"), help_text=_("When exam will be open for students."))
    end_time = models.DateTimeField(_("Ends at"), help_text=_("Responses after the deadline will not be accepted."))
    # timestamps
    updated_at = models.DateTimeField(_("Updated"), auto_now=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    @property
    def duration(self) -> int:
        """Exam duration in seconds."""
        return (self.end_time - self.start_time).seconds

    @property
    def duration_in_minutes(self) -> int:
        """Exam duration in minutes."""
        return int(self.duration / 60 + 0.5)

    @property
    def is_open(self) -> bool:
        """Can exam be attended now?"""
        now = timezone.now()
        return self.end_time > now > self.start_time

    def __str__(self) -> str:
        return self.title

    class Meta:
        verbose_name = _("Exam")
        verbose_name_plural = _("Exams")
        ordering = ("-updated_at",)


class ExamQuestion(models.Model):
    """M2M relation between Exams and Questions with extra details like points."""

    exam = models.ForeignKey(Exam, related_name="questions", on_delete=models.CASCADE, verbose_name=_("Exam"))
    question = models.ForeignKey(Question, related_name="exams", on_delete=models.CASCADE, verbose_name=_("Questions"))
    rank = models.IntegerField(_("Rank"), default=5, help_text=_("Ranks are used to order questions in the exam."))
    points = models.FloatField(_("Points"), default=10, help_text=_("Maximum points of the question."))
    syntax_penalty = models.FloatField(
        _("Max syntax penalty"), default=2, help_text=_("Maximum code syntax penalty, used for CODING questions.")
    )
    test_case_points = models.JSONField(
        _("Test case scores"),
        default=dict,
        blank=True,
        help_text=_(
            "You can provide custom score for each test case."
            "Use the name of the test case as the key, and points for that case as the value."
        ),
    )
    in_exam_results = models.BooleanField(
        _("Results during exam"), default=True, help_text=_("Show evaluation results of the answer during the exam.")
    )
    # timestamps
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    def __str__(self) -> str:
        return str(self.pk)

    class Meta:
        verbose_name = _("Exam question")
        verbose_name_plural = _("Exam questions")
        ordering = ("exam", "rank", "id")


class Response(models.Model):
    """Response for the exam question."""

    student = models.ForeignKey(User, related_name="responses", on_delete=models.CASCADE, verbose_name=_("Student"))
    exam_question = models.ForeignKey(
        ExamQuestion, related_name="responses", on_delete=models.CASCADE, verbose_name=_("Exam question")
    )
    solution = models.TextField(_("Solution"), blank=True, null=True)
    notes = models.TextField(_("Notes"), blank=True, null=True)
    # results
    total_score = models.FloatField(
        _("Total score"), blank=True, null=True, help_text=_("Score earned for this solution.")
    )
    score_details = models.JSONField(
        _("Score details"), default=dict, blank=True, help_text=_("Details provided by the auto grading system.")
    )
    # timestamps
    updated_at = models.DateTimeField(_("Updated"), auto_now=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    def __str__(self) -> str:
        return str(self.pk)

    @property
    def splitted_solution(self) -> List[str]:
        if not self.solution:
            return []
        return self.solution.split(",")

    class Meta:
        verbose_name = _("Response")
        verbose_name_plural = _("Responses")
        ordering = ("student__id", "-updated_at")
        constraints = [UniqueConstraint(name="unique_student_response", fields=["student", "exam_question"])]


class CodeEvaluationTracker(models.Model):
    id = models.UUIDField(_("Task id"), primary_key=True, default=uuid4, editable=False)
    response = models.ForeignKey(
        Response, related_name="code_evaluations", verbose_name=_("Response"), on_delete=models.CASCADE
    )
    raw_results = models.JSONField(
        _("Raw results"), default=dict, blank=True, help_text=_("Raw output of the evaluation.")
    )
    started_at = models.DateTimeField(_("Started"), blank=True, null=True)
    ended_at = models.DateTimeField(_("Ended"), blank=True, null=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    def __str__(self) -> str:
        return str(self.id)

    class Meta:
        verbose_name = _("Code evaluation tracker")
        verbose_name_plural = _("Code evaluation trackers")
        ordering = ("-created_at",)

import ast
import json
import shutil
from pathlib import Path
from time import sleep
from typing import Optional, Tuple
from uuid import uuid4

import docker
import requests
from django.conf import settings
from django.db.models import IntegerChoices
from django.shortcuts import reverse
from django.utils import timezone
from docker.errors import ContainerError

from dems.celery import app
from exam.models import CodeEvaluationTracker, Question, QuestionTypes, Response

# from docker.types import Mount


class EvaluationType(IntegerChoices):
    PYTEST = 1
    PYLINT = 2


def download_file(url: str, path: Path):
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with path.open("wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:
                    f.write(chunk)


def prepare_user_folder(folder_path: Path, question: Question, response: str, evaluation: int):
    user_code = folder_path / "user_code.py"
    with user_code.open("w") as user_code_file:
        user_code_file.write(response)
    if evaluation == EvaluationType.PYTEST:
        test_code = folder_path / "test_code.py"
        with test_code.open("w") as test_code_file:
            test_code_file.write(question.test_cases)
        for artifact in question.artifacts.all():
            download_url = reverse("web:artifact_download", args=[question.id, str(artifact.id)])
            download_url = settings.ARTIFACT_DOWNLOAD_DOMAIN + download_url
            artifact_path = folder_path / artifact.filename
            download_file(download_url, artifact_path)


def remove_user_folder(folder_path: Path):
    shutil.rmtree(folder_path)


def get_test_cases_count(test_cases: str) -> int:
    module = ast.parse(test_cases)
    counter = 0
    for node in module.body:
        if not isinstance(node, ast.FunctionDef):
            continue
        if node.name.startswith("test_"):
            counter += 1
    return counter


def grade_pytest_code(raw: dict, total_score: float, test_case_points: dict, test_case_count: int) -> Tuple[int, list]:
    score = 0
    reasons = []
    max_score = total_score
    # determine default score per test case
    if test_case_points:
        for case in test_case_points:
            case_points = int(test_case_points[case])
            total_score -= case_points
            test_case_count -= 1
        if not test_case_count:
            score_per_function = 0
        else:
            score_per_function = total_score / test_case_count
    else:
        score_per_function = total_score / test_case_count
    for test in raw.get("tests", []):
        test_name = test["nodeid"].replace("test_code.py::", "")
        if test.get("outcome", "unknown") == "passed":
            if test_name in test_case_points:
                # this test has custom score
                case_points = int(test_case_points[test_name])
            else:
                case_points = score_per_function
            score += case_points
        else:
            # save the issue with the crashed test in reasons
            reasons.append(f"{test_name} failed.")
    if score > max_score:
        score = max_score
    return score, reasons


@app.task(
    name="evaluate_user_response",
    bind=True,
    autoretry_for=(Exception,),
    retry_backoff=True,
    retry_kwargs={"max_retries": 2},
)
def evaluate_user_response(self, response_id: str, task_id: Optional[str] = None):
    tracker = None
    if task_id:
        tracker = CodeEvaluationTracker.objects.get(id=task_id)
        tracker.started_at = timezone.now()
        tracker.save()
    response: Response = (
        Response.objects.filter(id=response_id)
        .select_related("exam_question", "exam_question__question", "exam_question__exam")
        .first()
    )
    question_settings = response.exam_question
    # exam = question_settings.exam
    question = question_settings.question
    if question.question_type == QuestionTypes.CHOICE:
        if response.solution == question.correct_choice:
            response.total_score = question_settings.points
            response.score_details = {"result": "Correct choice"}
        else:
            response.total_score = 0
            response.score_details = {"result": "Incorrect choice"}
        response.save()
        if tracker:
            tracker.ended_at = timezone.now()
            tracker.save()
        return
    elif question.question_type == QuestionTypes.MULTI_CHOICE:
        correct_choices = [choice.strip() for choice in question.correct_choice.split(",")]
        coefficient = 1 / len(correct_choices)
        correct_responses = 0
        incorrect_responses = 0
        for choice in response.solution.split(","):
            choice = choice.strip()
            if choice in correct_choices:
                correct_responses += 1
            else:
                incorrect_responses += 1
        result = (correct_responses - incorrect_responses) * coefficient
        if result <= 0:
            response.total_score = 0
        else:
            response.total_score = round(question_settings.points * result, 0)
        response.score_details = {"result": f"Correct: {correct_responses}, Incorrect: {incorrect_responses}"}
        response.save()
        if tracker:
            tracker.ended_at = timezone.now()
            tracker.save()
        return
    elif question.question_type == QuestionTypes.CODE:
        # get a new folder for storing the code related files
        folder_path = settings.CODE_TESTING_PATH / str(uuid4())
        folder_path.mkdir()
        # prepare the folder
        prepare_user_folder(folder_path, question, response.solution, evaluation=EvaluationType.PYTEST)
        # evaluate the code
        client = docker.from_env()
        command = (
            "/bin/sh -c 'cd /app/files/; pytest --disable-pytest-warnings --json-report --json-report-omit "
            "keywords streams --json-report-file=/tmp/result.json . >> /dev/null; cat /tmp/result.json'"
        )
        # mount_directory = Mount(target="/app/files", source=folder_path.as_posix(), read_only=True)
        volumes = {folder_path.as_posix(): {"bind": "/app/files", "mode": "ro"}}
        try:
            runner = client.containers.run(settings.PYTEST_DOCKER_IMAGE, command, volumes=volumes, detach=True)
            counter = 5
            while counter:
                result = runner.logs()
                if result:
                    break
                sleep(3)
                counter -= 1
            else:
                response.total_score = 0
                response.score_details = {"pytest": "execution failed"}
                response.save()
                if tracker:
                    tracker.raw_results = {"problem": "No container output during wait time.", "execution": "failed"}
                    tracker.ended_at = timezone.now()
                    tracker.save()
                raise Exception("No container output during wait time.")
        except ContainerError:
            response.total_score = 0
            response.score_details = {"pytest": "execution failed"}
            response.save()
            if tracker:
                tracker.raw_results = {"problem": "Container had an error.", "execution": "failed"}
                tracker.ended_at = timezone.now()
                tracker.save()
            raise Exception("Container gave an error")
        # convert the result to dict
        result = result.decode("utf-8")
        result = json.loads(result)
        # check for the exit code
        if "exitcode" not in result or result["exitcode"] not in [0, 1]:
            response.total_score = 0
            response.score_details = {"pytest": "unknown exit code"}
            response.save()
            if tracker:
                tracker.raw_results = {"response": result, "problem": "incorrect exit code", "execution": "failed"}
                tracker.ended_at = timezone.now()
                tracker.save()
            return
        # evaluate the result
        total_test_cases = get_test_cases_count(question.test_cases)
        if not total_test_cases:
            response.total_score = 0
            response.score_details = {"pytest": "no test cases found"}
            response.save()
            if tracker:
                tracker.raw_results = {"output": result, "execution": "success", "problems": "No test functions."}
                tracker.ended_at = timezone.now()
                tracker.save()
        score, details = grade_pytest_code(
            result, question_settings.points, question_settings.test_case_points, total_test_cases
        )
        response.total_score = score
        response.score_details = {"pytest": details}
        response.save()
        # save the tracker output
        if tracker:
            tracker.raw_results = {"output": result, "execution": "success", "score": score}
            tracker.ended_at = timezone.now()
            tracker.save()
        # remove the folder
        remove_user_folder(folder_path)
        return
    return

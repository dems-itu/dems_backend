DEMS
====

 [![pipeline status](https://gitlab.com/dems-itu/dems_backend/badges/master/pipeline.svg)](https://gitlab.com/dems-itu/dems_backend/-/pipelines)
 [![coverage report](https://gitlab.com/dems-itu/dems_backend/badges/master/coverage.svg)](https://dems-itu.gitlab.io/dems_backend/)

Digital Exam Management System

Online programming exam system with auto-grading options, focused on Python exams with a linter and test cases.

# INSTALL

The project requires [Poetry](https://python-poetry.org/) dependency management tool.
Install it in your operating system before installing the project.

```bash
# clone the repository
$ git clone git@gitlab.com:dems-itu/dems_backend.git
# go to the project directory
$ cd dems_backend
# install project dependencies
$ poetry install
# create the configuration file from a template
cp .env.template .env
# edit the .env file to adjust for your needs
# activate the virtual shell
$ poetry shell
# initialize/update the database with migrations
$ ./manage.py migrate
# test the project
$ ./manage.py test
# create an admin user
$ ./manage.py createsuperuser
# run the test server
$ ./manage.py runserver
# access your test server at 127.0.0.1:8000/
# access the admin interface at 127.0.0.1:8000/admin/
```

# DEPLOYMENT

@TODO

# COPYRIGHT

MIT License. Copyright (c) 2020 Emin Mastizada.

Check the LICENSE file provided with the project for the details.


from django.contrib import admin
from django.urls import include, path
from django.utils.translation import ugettext_lazy as _

urlpatterns = [
    path("admin/", admin.site.urls),
    path("markdownx/", include("markdownx.urls")),
    path("", include(("web.urls", "web"), namespace="web")),
]


admin.site.site_header = _("DEMS Admin")
admin.site.site_title = _("DEMS Admin")
admin.site.index_title = _("Welcome to the Digital Exam Management System")

from pathlib import Path

import environ
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Prepare the environment
env = environ.Env(
    # casting and default values
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, []),
    DATABASE_URL=(str, f"sqlite:///{BASE_DIR}/db.sqlite3"),
    USE_I18N=(bool, True),
    USE_L10N=(bool, True),
    USE_TZ=(bool, True),
    EXTRA_APPS=(list, []),
    CELERY_TASK_ALWAYS_EAGER=(bool, True),
    CELERY_TASK_TIME_LIMIT=(int, 900),
)
env.read_env(env_file=f"{BASE_DIR}/.env")

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("SECRET_KEY", default="NotSo@Secret")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DEBUG")

ALLOWED_HOSTS = env("ALLOWED_HOSTS")


# Application definition

INSTALLED_APPS = [
    "user",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "markdownx",
    "django_ace",
    "django_json_widget",
    "reversion",
    "classroom",
    "exam",
    "web",
]

# add extra apps from the configuration
for app in env("EXTRA_APPS"):
    INSTALLED_APPS.append(app)

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "reversion.middleware.RevisionMiddleware",
]

ROOT_URLCONF = "dems.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": ["web/templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "dems.wsgi.application"
AUTH_USER_MODEL = "user.User"
PAGINATION_LIMIT = 25
LOGIN_URL = "/login/"
LOGOUT_REDIRECT_URL = "/"
FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o755
FILE_UPLOAD_PERMISSIONS = 0o644
APPEND_SLASH = True

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {"default": env.db()}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/
LANGUAGES = (
    ("tr", _("Turkish")),
    ("en", _("English")),
)
LOCALE_PATHS = (BASE_DIR / "locale",)

LANGUAGE_CODE = env("LANGUAGE_CODE", default="en")

TIME_ZONE = env("TIME_ZONE", default="Europe/Istanbul")

USE_I18N = env("USE_I18N")
USE_L10N = env("USE_L10N")
USE_TZ = env("USE_TZ")

# Celery
CELERY_TASK_TRACK_STARTED = True
CELERY_TIMEZONE = TIME_ZONE
CELERY_TASK_TIME_LIMIT = env("CELERY_TASK_TIME_LIMIT")  # 15 minutes
CELERY_BROKER_URL = env("CELERY_BROKER_URL")
CELERY_TASK_ALWAYS_EAGER = env("CELERY_TASK_ALWAYS_EAGER")

# Test related values
CODE_TESTING_PATH = Path(env("CODE_TESTING_PATH", default="/tmp/"))
ARTIFACT_DOWNLOAD_DOMAIN = env("ARTIFACT_DOWNLOAD_DOMAIN", default="http://127.0.0.1")
PYTEST_DOCKER_IMAGE = env("PYTEST_DOCKER_IMAGE", default="mastizada/demspytest")
PYLINT_DOCKER_IMAGE = env("PYLINT_DOCKER_IMAGE", default="mastizada/demspylint")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = "{base}/{static}".format(base=BASE_DIR, static=STATIC_URL.strip("/"))

MEDIA_URL = "/media/"
MEDIA_ROOT = "{base}/{static}".format(base=BASE_DIR, static=MEDIA_URL.strip("/"))

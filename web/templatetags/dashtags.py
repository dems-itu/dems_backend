from typing import Optional

from django import template
from markdownx.utils import markdownify

from exam.models import ExamQuestion, Response
from user.models import User

register = template.Library()


@register.filter
def question_response(user: User, question: ExamQuestion) -> Optional[Response]:
    return Response.objects.filter(student=user, exam_question=question).first()


@register.filter
def markdown_to_html(content: str) -> str:
    return markdownify(content)

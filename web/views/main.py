"""
Pages like homepage, about, login, and register.
"""
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import FormView, TemplateView

from user.models import User
from web.forms.user import LoginForm


class HomepageView(TemplateView):
    template_name = "index.html"


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("web:homepage"))


class LoginView(FormView):
    template_name = "user/login.html"
    form_class = LoginForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["next_url"] = self.request.GET.get("next")
        return kwargs

    def form_valid(self, form):
        username = form.cleaned_data.get("username")
        user = User.objects.filter(username=username).first()
        next_url = form.cleaned_data.get("next_url")
        login(self.request, user, backend="django.contrib.auth.backends.ModelBackend")
        if form.cleaned_data.get("remember_me"):
            self.request.session.set_expiry(0)
        return HttpResponseRedirect(self.get_success_url(next_url=next_url, user=user))

    def get_success_url(self, next_url=None, user=None):
        if next_url:
            return next_url
        return reverse("web:homepage")

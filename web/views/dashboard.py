"""
Student dashboard pages.
"""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic import TemplateView

from classroom.models import Classroom
from exam.models import Exam


class StudentDashboardView(LoginRequiredMixin, TemplateView):
    template_name = "student/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_slug"] = "dashindex"
        return context


class StudentClassroomsView(LoginRequiredMixin, TemplateView):
    template_name = "student/classrooms.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_slug"] = "classrooms"
        classroom_filter = self.request.GET.get("classroom")
        classrooms = self.request.user.enrollments.all().prefetch_related("teachers", "students")
        if classroom_filter:
            classrooms = classrooms.filter(id=classroom_filter)
        context["classrooms"] = classrooms.distinct()
        return context


class StudentExamsView(LoginRequiredMixin, TemplateView):
    template_name = "student/exams.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_slug"] = "exams"
        classroom_filter = self.request.GET.get("classroom")
        exams = Exam.objects.filter(
            classroom__students=self.request.user, end_time__gte=timezone.now()
        ).select_related("classroom")
        classroom = None
        if classroom_filter:
            classroom = get_object_or_404(Classroom, pk=classroom_filter)
            exams = exams.filter(classroom=classroom)
        context["exams"] = exams.distinct()
        context["classroom"] = classroom
        return context

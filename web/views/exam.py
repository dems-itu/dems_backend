"""
The exam page that will be used by students.
"""
from uuid import uuid4

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404, HttpResponse, JsonResponse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView, View

from exam.models import CodeEvaluationTracker, Exam, ExamQuestion, QuestionArtifact, QuestionTypes, Response
from exam.tasks import evaluate_user_response
from web.forms.question import CodeResponseForm


class ExamHomeView(LoginRequiredMixin, TemplateView):
    """List of questions for the selected exam."""

    template_name = "exam/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_slug"] = "exams"
        exam_id = kwargs.get("exam_id")
        exam = (
            Exam.objects.filter(id=exam_id, classroom__students=self.request.user)
            .select_related("classroom")
            .prefetch_related("questions", "questions__question")
            .first()
        )
        if not exam:
            raise Http404(_("Exam does not exist!"))
        if not exam.is_open:
            raise PermissionDenied(_("Exam has not started yet!"))
        context["exam"] = exam
        return context


class QuestionView(LoginRequiredMixin, TemplateView):
    """Show question details and the response form."""

    template_name = "exam/question.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_slug"] = "exams"
        exam_id = kwargs.get("exam_id")
        question_id = kwargs.get("question_id")
        question = (
            ExamQuestion.objects.filter(id=question_id, exam__id=exam_id, exam__classroom__students=self.request.user)
            .select_related("question", "exam")
            .prefetch_related("exam__questions", "exam__questions__question")
            .first()
        )
        response = Response.objects.filter(student=self.request.user, exam_question=question).first()
        response_form = None
        if question.question.question_type == QuestionTypes.CODE:
            if response:
                response_form = CodeResponseForm(initial={"response": response.solution})
            elif question.question.test_template:
                response_form = CodeResponseForm(initial={"response": question.question.test_template})
            else:
                response_form = CodeResponseForm()
        context["exam"] = question.exam
        context["question"] = question
        context["question_types"] = QuestionTypes
        context["response"] = response
        context["response_form"] = response_form
        return context


class QuestionEvaluationView(LoginRequiredMixin, View):
    """Save the provided response and start the task if needed."""

    @staticmethod
    def get(request, *args, **kwargs):
        return JsonResponse({"status": "ready"}, safe=True, status=200)

    @staticmethod
    def post(request, *args, **kwargs):
        """Save a response to a question."""
        question_id = kwargs["question_id"]
        # get details for the question
        question = (
            ExamQuestion.objects.filter(id=question_id, exam__classroom__students=request.user)
            .select_related("exam", "question")
            .first()
        )
        if not question:
            return JsonResponse({"status": "error", "message": _("Question was not found!")}, safe=True, status=400)
        # check for the deadline
        if timezone.now() > question.exam.end_time:
            return JsonResponse(
                {"status": "error", "message": _("Submitting changes after the deadline is not allowed!")},
                safe=True,
                status=403,
            )
        solution = request.POST.get("solution")
        if not solution:
            return JsonResponse(
                {"status": "error", "message": _("The provided solution is empty.")}, safe=True, status=400
            )
        # make basic checks before storing the response
        if question.question.question_type == QuestionTypes.TEXT:
            if len(solution) > question.question.max_text_length:
                return JsonResponse(
                    {
                        "status": "error",
                        "message": _(f"Response cannot be longer than {question.question.max_text_length}."),
                    },
                    safe=True,
                    status=400,
                )
        elif question.question.question_type == QuestionTypes.CHOICE:
            if solution not in question.question.choices.keys():
                return JsonResponse(
                    {"status": "error", "message": _("The provided option is unknown.")}, safe=True, status=400
                )
        elif question.question.question_type == QuestionTypes.MULTI_CHOICE:
            if not all([sub_solution in question.question.choices.keys() for sub_solution in solution.split(",")]):
                return JsonResponse(
                    {"status": "error", "message": _("One of the provided options in unknown.")}, safe=True, status=400
                )
        elif question.question.question_type == QuestionTypes.CODE:
            # @TODO perform code safety checks
            pass
        # check if there is a response for this question
        response = Response.objects.filter(student=request.user, exam_question=question).first()
        if response:
            response.solution = solution
            response.total_score = 0
            response.score_details = {}
            response.save()
        else:
            response = Response.objects.create(student=request.user, exam_question=question, solution=solution)

        # Schedule a grading task
        task_id = None
        if question.in_exam_results:
            task_id = uuid4()
            CodeEvaluationTracker.objects.create(id=task_id, response=response)
            transaction.on_commit(
                lambda: evaluate_user_response.apply_async(kwargs={"response_id": response.id, "task_id": task_id})
            )

        # return response
        if question.question.question_type in [QuestionTypes.CHOICE, QuestionTypes.MULTI_CHOICE]:
            return JsonResponse({"status": "success", "message": _("Solution saved.")}, safe=True, status=200)
        elif question.question.question_type == QuestionTypes.CODE:
            if question.in_exam_results:
                return JsonResponse(
                    {"status": "success", "message": _("Code evaluation started..."), "task_id": task_id},
                    safe=True,
                    status=200,
                )
            return JsonResponse({"status": "success", "message": _("Solution saved.")}, safe=True, status=200)
        # text or anything else
        return JsonResponse({"status": "success", "message": _("Solution saved.")}, safe=True, status=200)

    @staticmethod
    def delete(request, *args, **kwargs):
        """Delete a response for a question."""
        question_id = kwargs["question_id"]
        # get details for the question
        question = (
            ExamQuestion.objects.filter(id=question_id, exam__classroom__students=request.user)
            .select_related("exam", "question")
            .first()
        )
        # check for the deadline
        if timezone.now() > question.exam.end_time:
            return JsonResponse(
                {"status": "error", "message": _("Submitting changes after the deadline is not allowed!")},
                safe=True,
                status=403,
            )
        if not question:
            return JsonResponse({"status": "error", "message": _("Question was not found!")}, safe=True, status=400)
        # check if there is a response for this question
        response = Response.objects.filter(student=request.user, exam_question=question).first()
        if not response:
            return JsonResponse({"status": "success", "message": _("Nothing to remove.")}, safe=True, status=200)
        response.delete()
        return JsonResponse({"status": "success", "message": _("Solution removed.")}, safe=True, status=200)


class EvaluationResultView(LoginRequiredMixin, View):
    @staticmethod
    def get(request, *args, **kwargs):
        evaluation_uuid = kwargs["evaluation_uuid"]
        tracker: CodeEvaluationTracker = (
            CodeEvaluationTracker.objects.filter(id=evaluation_uuid)
            .select_related("response", "response.exam_question")
            .first()
        )
        if not tracker:
            return JsonResponse({"status": "not_found", "message": _("Task was not found.")}, safe=True, status=404)
        if not tracker.ended_at:
            return JsonResponse(
                {"status": "waiting", "message": _("Task has not finished yet.")}, safe=True, status=202
            )
        # we have a result
        question_settings = tracker.response.exam_question
        show_results = question_settings.in_exam_results
        show_execution_warning = True
        execution_failed = False
        if "execution" in tracker.raw_results and tracker.raw_results["execution"] == "failed":
            execution_failed = True
        if execution_failed and show_execution_warning:
            return JsonResponse({"status": "completed", "message": _("Code failed")}, safe=True, status=400)
        # code did not fail
        if show_results:
            return JsonResponse(
                {"status": "completed", "message": _(f"Points: {tracker.response.total_score}")}, safe=True, status=200
            )
        return JsonResponse({"status": "completed", "message": _("Code was tested and saved.")}, safe=True, status=200)


class QuestionArtifactDownloadView(View):
    @staticmethod
    def get(request, *args, **kwargs):
        question_id = kwargs["question_id"]
        artifact_uuid = kwargs["artifact_uuid"]
        artifact = QuestionArtifact.objects.filter(id=artifact_uuid, question__id=question_id).first()
        response = HttpResponse(artifact.file, content_type="text/plain")
        response["Content-Disposition"] = f"attachment; filename={artifact.filename}"
        return response

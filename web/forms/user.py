from django import forms
from django.contrib.auth.hashers import check_password
from django.utils.translation import ugettext_lazy as _

from user.models import User


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=150,
        label=_("Username"),
        required=True,
        widget=forms.TextInput(attrs={"placeholder": _("Username"), "class": "form-control"}),
    )
    password = forms.CharField(
        label=_("Password"),
        max_length=50,
        required=True,
        widget=forms.PasswordInput(attrs={"placeholder": _("Password"), "class": "form-control"}),
    )
    remember_me = forms.BooleanField(initial=False, widget=forms.HiddenInput(), required=False)
    next_url = forms.CharField(max_length=254, widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        try:
            self.next_url_kw = kwargs.pop("next_url")
        except KeyError:
            self.next_url_kw = ""
        super().__init__(*args, **kwargs)
        self.fields["next_url"].initial = self.next_url_kw

    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        user = User.objects.filter(username=username).first()
        if not user:
            self.add_error("username", _("User with that username is not registered!"))
        elif not check_password(password, user.password):
            self.add_error("password", _("Incorrect password"))

from django import forms
from django_ace import AceWidget


class CodeResponseForm(forms.Form):
    response = forms.CharField(widget=AceWidget(theme="dracula", mode="python"))

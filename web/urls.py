from django.urls import path

from web.views.dashboard import StudentClassroomsView, StudentDashboardView, StudentExamsView
from web.views.exam import ExamHomeView, QuestionArtifactDownloadView, QuestionEvaluationView, QuestionView
from web.views.main import HomepageView, LoginView, logout_view

urlpatterns = [
    path("", HomepageView.as_view(), name="homepage"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", logout_view, name="logout"),
    path("dash/classrooms/", StudentClassroomsView.as_view(), name="classrooms"),
    path(
        "dash/exams/<int:exam_id>/<int:question_id>/evaluate/",
        QuestionEvaluationView.as_view(),
        name="question_evaluation_ajax",
    ),
    path("dash/exams/<int:exam_id>/<int:question_id>/", QuestionView.as_view(), name="question_page"),
    path("dash/exams/<int:exam_id>/", ExamHomeView.as_view(), name="exam_page"),
    path("dash/exams/", StudentExamsView.as_view(), name="exams_dashboard"),
    path("dash/", StudentDashboardView.as_view(), name="student_dashboard"),
    path(
        "download/<int:question_id>/<uuid:artifact_uuid>/",
        QuestionArtifactDownloadView.as_view(),
        name="artifact_download",
    ),
]

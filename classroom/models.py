from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from user.models import User


class DegreeTypes(models.TextChoices):
    OTHER = "other", _("Other")
    BACHELOR = "bachelor", _("Bachelor")
    MASTER = "master", _("Master")
    DOCTORATE = "doctorate", _("Doctorate")


class Classroom(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    degree = models.CharField(_("Degree"), max_length=50, choices=DegreeTypes.choices, default=DegreeTypes.OTHER)
    course_code = models.CharField(_("Course code"), max_length=50, blank=True, null=True)
    teachers = models.ManyToManyField(
        User,
        related_name="lectures",
        verbose_name=_("Teachers"),
        limit_choices_to=Q(is_teacher=True) | Q(is_assistant=True),
    )
    students = models.ManyToManyField(User, related_name="enrollments", verbose_name=_("Students"))
    # timestamps
    updated_at = models.DateTimeField(_("Updated"), auto_now=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    @property
    def teacher_count(self) -> int:
        return self.teachers.count()

    @property
    def printable_teachers(self) -> str:
        return ", ".join([teacher.name_only for teacher in self.teachers.all()])

    @property
    def students_count(self) -> int:
        return self.students.count()

    @property
    def upcoming_exam(self):
        return self.exams.filter(end_time__gte=timezone.now()).order_by("start_time").first()

    def __str__(self) -> str:
        if self.course_code:
            return f"{self.name} ({self.course_code})"
        return self.name

    class Meta:
        verbose_name = _("Classroom")
        verbose_name_plural = _("Classrooms")
        ordering = ("-created_at",)

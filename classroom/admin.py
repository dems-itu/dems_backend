from django.contrib import admin
from reversion.admin import VersionAdmin

from classroom.models import Classroom


@admin.register(Classroom)
class ClassroomAdmin(VersionAdmin):
    list_display = ("name", "course_code", "degree", "students_count", "updated_at", "created_at")
    list_filter = ("updated_at", "created_at")
    autocomplete_fields = ("teachers", "students")
    search_fields = ("name", "course_code")

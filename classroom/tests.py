from django.test import TestCase
from django.utils import timezone

from classroom.apps import ClassroomConfig
from classroom.models import Classroom, DegreeTypes
from user.models import User


class ClassroomTestCase(TestCase):
    CRN = "1000101"
    CODE = "BLG101"

    def test_apps(self):
        self.assertEqual(ClassroomConfig.name, "classroom")

    def test_create_classroom(self):
        classroom = Classroom.objects.create(name=self.CRN)
        self.assertEqual(classroom.__str__(), self.CRN)
        self.assertEqual(classroom.teacher_count, 0)
        self.assertEqual(classroom.students_count, 0)
        self.assertEqual(classroom.degree, DegreeTypes.OTHER)
        self.assertIsNone(classroom.course_code)
        classroom.course_code = self.CODE
        classroom.save()
        self.assertEqual(classroom.__str__(), f"{self.CRN} ({self.CODE})")
        self.assertTrue(isinstance(classroom.created_at, type(timezone.now())))
        teacher = User.objects.create_user(username="0123456", is_teacher=True)
        assistant = User.objects.create_user(username="0123457", is_assistant=True)
        student = User.objects.create_user(username="123457")
        classroom.teachers.add(teacher, assistant)
        classroom.students.add(student)
        self.assertEqual(classroom.teacher_count, 2)
        self.assertEqual(classroom.students_count, 1)
        self.assertEqual(classroom.exams.count(), 0)
